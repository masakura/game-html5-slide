class SampleLoader {
    constructor(canvas) {
        const renderer = new THREE.WebGLRenderer({
            canvas: canvas
        });
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(960 * 2, 540 * 2);

        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(45, 960 / 540, 0.1, 10000);
        camera.position.set(0, 10, 10);
        const controls = new THREE.OrbitControls(camera);
        controls.target.set(0, 3, 0);
        controls.update();
        const directionalLight = new THREE.DirectionalLight(0xFFFFFF);
        directionalLight.position.set(1, 1, 1);
        scene.add(directionalLight);
        const ambienLight = new THREE.AmbientLight(0x333333);
        scene.add(ambienLight);
        this._renderer = renderer;
        this._camera = camera;
        this._scene = scene;

        this.stretch();
    }

    initialize() {
        window.addEventListener('resize', () => this.stretch(), false);
        this.load();
    }

    render() {
        this._renderer.render(this._scene, this._camera);
    }

    load() {
        const loader = new THREE.ColladaLoader();
        loader.load('https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/models/collada/elf/elf.dae',
                collada => this._scene.add(collada.scene));
    }

    stretch() {
        const size = {
            width: window.innerWidth,
            height: window.innerHeight,
            aspect() {
                return this.width / this.height;
            }
        };

        this._camera.aspect = size.aspect();
        this._camera.updateProjectionMatrix();

        this._renderer.setSize(size.width, size.height);
    }
}