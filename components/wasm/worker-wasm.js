fetch('./prime.wasm')
    .then(response => response.arrayBuffer())
    .then(bytes => WebAssembly.instantiate(bytes, {
        'global': {},
        'env': {
            STACKTOP: 0,
            STACK_MAX: 256,
            abortStackOverflow: function (i32) { console.log("stack oveflow"); },
            memory: new WebAssembly.Memory({initial: 10, limit: 100}),
            table: new WebAssembly.Table({initial: 100, element: 'anyfunc'}),
            memoryBase: 0,
            tableBase: 0,
            abort() {
                console.log('abort');
            }
        }
    }))
    .then(results => {
        console.log(results.instance.exports._prime());
        self.postMessage({});;
    });
