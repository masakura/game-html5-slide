function calclator() {
    "use asm";

    function calc() {
        var last = 0;
        var i = 2;
        var num = 100000;
        var y = 0;
        var j = 0;
        var r = 0;

        for (; (i|0) <= (num|0); i = (i + 1)|0) {
            y = 0;
            j = 1;
            for(; (j|0) <= (i|0); j = (j + 1)|0) {
                r = ((i|0) % (j|0))|0;
                if ((r|0) == 0) y = (y + 1)|0;
            }

            if ((y|0) == 2) last = i|0;
        }

        return last|0;
    }

    return {
        calc: calc
    }
}

const result = calclator().calc();
console.log(result);
self.postMessage({});