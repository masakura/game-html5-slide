(async () => {
    class VideoStream {
        constructor($video) {
            this._$video = $video;
        }

        async initialize() {
            const stream = await navigator.mediaDevices.getUserMedia({
                video: {
                    faceMode: 'user'
                }, audio: false
            });
            this._$video.src = window.URL.createObjectURL(stream);
        }

        start() {
            this._$video.play();
        }

        stop() {
            this._$video.pause();
        }
    }

    class Tracker {
        constructor() {
            const tracker = new clm.tracker();
            tracker.init(pModel);

            this._tracker = tracker;
        }

        connect(source) {
            this._source = source;
        }

        start() {
            this._tracker.start(this._source._$video);
        }

        stop() {
            this._tracker.stop();
        }

        getFace() {
            const positions = this._tracker.getCurrentPosition();
            if (!positions) return false;
            return new Face(positions);
        }
    }

    class Face {
        constructor(positions) {
            this._positions = positions;
        }

        get mouse() {
            return new Mouse(this._positions);
        }

        get nose() {
            return new Nose(this._positions);
        }
    }

    class Mouse {
        constructor(positions) {
            this._positions = positions;
        }

        get size() {
            const mouseSize = this._positions[57][1] - this._positions[60][1];
            let a = mouseSize / 8;
            if (a > 1) a = 1;
            if (a < 0) a = 0;
            return a;
        }
    }

    class Nose {
        constructor(positions) {
            this._positions = positions;
        }

        get top() {
            return {x: this._positions[33][0], y: this._positions[33][1]};
        }

        get bottom() {
            return {x: this._positions[62][0], y: this._positions[62][1]};
        }

        get width() {
            return this.bottom.x - this.top.x;
        }

        get height() {
            return this.bottom.y - this.top.y;
        }

        get slant() {
            return this.width / this.height;
        }

        get angle() {
            return Math.atan(this.slant) / 2;
        }
    }

    class Character {
        constructor($canvas) {
            const mmd = new MMD($canvas, $canvas.width, $canvas.height);
            mmd.initShaders();
            mmd.initParameters();
            mmd.registerDragListener(document);

            this._mmd = mmd;
        }

        load(filename) {
            const model = new MMD.Model('model', filename);
            this._model = model;

            const mmd = this._mmd;
            return new Promise(resolve => {
                model.load(() => {
                    mmd.addModel(model);
                    mmd.initBuffers();
                    mmd.start();

                    resolve();
                })
            })
        }

        update(face) {
            if (!face) return;

            const model = this._model;
            const mmd = this._mmd;
            mmd.moveMorphs(model, this.morphs(face));
            mmd.moveBones(model, this.bones(face));
            mmd.playing = true;
            mmd.render();
        }

        morphs(face) {
            return {'あ': face.mouse.size};
        }

        bones(face) {
            const location = new Float32Array(3);
            const rotation = new Float32Array(4);
            rotation[0] = 0; // x
            rotation[1] = 0; // y
            rotation[2] = face.nose.angle; // z
            rotation[3] = 1;

            return {
                '頭': {location, rotation}
            }
        }
    }

    const source = new VideoStream(document.querySelector('#video'));
    await source.initialize();

    const tracker = new Tracker();
    tracker.connect(source);

    const character = new Character(document.querySelector('#canvas'));
    await character.load('Miku_Hatsune_Ver2.pmd');

    class Renderer {
        initialize() {
        }

        render() {
            const face = tracker.getFace();
            character.update(face);
        }

        start() {
            source.start();
            tracker.start();
        }

        stop() {
            tracker.stop();
            source.stop();
        }
    }
    
    const delay = new Delay(new Renderer());
    delay.bind(window);
})();