export default class Rectangle {
    constructor(left, top, right, bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    get horizontal() {
        return {
            low: this.left,
            high: this.right
        }
    }

    get vertical() {
        return {
            low: this.bottom,
            high: this.top
        }
    }

    insideHorizontal(position) {
        if (this.left >= position.x) {
//            position.x = this.left;
            return false;
        } else if(this.right <= position.x) {
//            position.x = this.right;
            return false;
        }
        return true;
    }

    insideVertical(position) {
        if (this.bottom >= position.y) {
//            position.y = this.bottom;
            return false;
        } else if (this.top <= position.y) {
//            position.y = this.top;
            return false;
        }
        return true;

    }

    /**
     * @param {Size} size
     */
    shrink(size) {
        const half = size.half();
        return new Rectangle(this.left + half.width, this.top - half.height, this.right - half.width, this.bottom + half.height);
    }

    /**
     * @param {Size} size
     */
    static from(size) {
        const half = size.half();

        return new Rectangle(-half.width, half.height, half.width, -half.height);
    }
}