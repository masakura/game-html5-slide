export default class PlayerAnimation {
    /**
     * @param {GamePlayer} obj
     */
    constructor(obj) {
        this._obj = obj;
    }

    /**
     * @param {Input} input
     */
    update(input) {
        switch (input.horizontal) {
            case 0: this._obj.rest(); break;
            case 1: this.move(false); break;
            case -1: this.move(true); break;
        }
    }

    move(negative) {
        if (this._obj.bottom) {
            this._obj.walk(negative);
        } else {
            this._obj.swim(negative);
        }
    }
}