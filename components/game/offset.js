export default class Offset {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    update(position) {
        position.x += this.x;
        position.y += this.y;
    }

    static onlyX(x) {
        return new Offset(x, 0);
    }

    static onlyY(y) {
        return new Offset(0, y);
    }
}