export default class Size {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    half() {
        return new Size(this.width / 2, this.height / 2);
    }

    get aspect() {
        return this.width / this.height;
    }

    /**
     * @param $canvas
     * @returns {Size}
     */
    static from($canvas) {
        return new Size($canvas.width, $canvas.height);
    }
}