export class Velocity {
    /**
     * @returns {number}
     */
    get value() {
        throw new Error('Require Implement.');
    }

    /**
     * @param {number} value
     */
    set value(value) {
        throw new Error('Require Implement.');
    }

    /**
     * @param {number} value
     */
    add(value) {
        this.value += value;
    }

    reset() {
        this.value = 0;
    }

    /**
     * @param {Position} position
     */
    update(position) {
        position.offset(this.value);
    }

    /**
     * @param {number} value
     * @returns {Velocity}
     */
    limit(value) {
        return new LimitVelocity(this, value);
    }

    /**
     * @param {number} value
     * @returns {Velocity}
     */
    resistor(value) {
        return new ResistorVelocity(this, value);
    }

    /**
     * @param {number} value
     * @returns {Velocity}
     */
    acceleration(value) {
        return new AccelerationVelocity(this, value);
    }

    /**
     * @returns {Velocity}
     */
    static create() {
        return new SimpleVelocity();
    }
}

export class SimpleVelocity extends Velocity {
    constructor() {
        super();

        this._value = 0
    }

    get value() {
        return this._value;
    }

    set value(value) {
        this._value = value;
    }
}

export class LimitVelocity extends Velocity {
    /**
     * @param {Velocity} parent
     * @param {number} limit
     */
    constructor(parent, limit) {
        super();

        this._parent = parent;
        this._limit = limit;
    }

    get value() {
        return this._parent.value;
    }

    set value(value) {
        this._parent.value = this.fit(value);
    }

    /**
     * @param {number} value
     * @returns {number}
     */
    fit(value) {
        if (!this._limit) return value;

        if (value > this._limit) return this._limit;
        if (value < -this._limit) return -this._limit;
        return value;
    }
}

export class ResistorVelocity extends Velocity {
    /**
     * @param {Velocity} parent
     * @param {number} value
     */
    constructor(parent, value) {
        super();

        this._parent = parent;
        this._value = value;
    }

    get value() {
        return this._parent.value;
    }

    set value(value) {
        this._parent.value = value;
    }

    update(position) {
        const value = this._parent.value;
        const sign = value > 0 ? 1 : (value < 0 ? -1 : 0);

        if (sign === 0) return;

        let abs = Math.abs(value);
        abs -= this._value;
        if (abs < 0) abs = 0;

        this._parent.value = abs * sign;

        this._parent.update(position);
    }
}

export class AccelerationVelocity extends Velocity {
    /**
     * @param {Velocity} parent
     * @param {number} value
     */
    constructor(parent, value) {
        super();

        this._parent = parent;
        this._value = value;
    }

    get value() {
        return this._parent.value;
    }

    set value(value) {
        this._parent.value = value;
    }

    update(position) {
        this.add(this._value);

        this._parent.update(position);
    }
}

export class Velocity2D {
    /**
     * @returns {Velocity}
     */
    get x() {
        throw new Error('Require Implement.');
    }

    /**
     * @returns {Velocity}
     */
    get y() {
        throw new Error('Require Implement.');
    }

    update() {
        this.x.update(this._position().x);
        this.y.update(this._position().y);
    }

    /**
     * @param {Function} position
     * @returns {Velocity2D}
     */
    bindPosition(position) {
        this._position = position;
        return this;
    }

    /**
     * @param {{x: {number}, y: {number}}} value
     * @returns {Velocity2D}
     */
    limit(value) {
        return new LimitVelocity2D(this, value);
    }

    /**
     * @param {Object} value
     * @param {number} value.x
     * @param {number} value.y
     * @returns {Velocity2D}
     */
    resistor(value) {
        return new ResistorVelocity2D(this, value);
    }

    /**
     * @param {Object} value
     * @param {number} value.x
     * @param {number} value.y
     * @returns {Velocity2D}
     */
    acceleration(value) {
        return new AccelerationVelocity2D(this, value);
    }

    /**
     * @param {number} value
     * @returns {Velocity2D}
     */
    gravity(value) {
        return this.acceleration({x: 0, y: -value});
    }

    /**
     * @returns {Velocity2D}
     */
    static create() {
        return new SimpleVelocity2D();
    }
}

export class SimpleVelocity2D extends Velocity2D {
    constructor() {
        super();

        this._x = new SimpleVelocity(5);
        this._y = new SimpleVelocity();
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }
}

export class LimitVelocity2D extends Velocity2D {
    /**
     * @param {Velocity2D} parent
     * @param {Object} value
     * @param {number} value.x
     * @param {number} value.y
     */
    constructor(parent, value) {
        super();

        this._x = parent.x.limit(value.x);
        this._y = parent.y.limit(value.y);
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }
}

class ResistorVelocity2D extends Velocity2D {
    /**
     * @param {Velocity2D} parent
     * @param {Object} value
     * @param {number} value.x
     * @param {number} value.y
     */
    constructor(parent, value) {
        super();

        this._x = parent.x.resistor(value.x);
        this._y = parent.y.resistor(value.y);
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }
}

class AccelerationVelocity2D extends Velocity2D {
    /**
     * @param {Velocity2D} parent
     * @param {Object} value
     * @param {number} value.x
     * @param {number} value.y
     */
    constructor(parent, value) {
        super();

        this._x = parent.x.acceleration(value.x);
        this._y = parent.y.acceleration(value.y);
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }
}