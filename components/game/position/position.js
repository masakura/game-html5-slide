export class Position {
    /**
     * @returns {number}
     */
    get value() {
        throw new Error('Not Implement');
    }

    /**
     * @param {number} value
     */
    set value(value) {
        throw new Error('Not Implement');
    }

    /**
     * @param {number} value
     */
    offset(value) {
        this.value += value;
    }

    /**
     * @param {{low: {number}, high: {number}}} range
     * @param {Velocity} velocity
     * @param {Function} outline
     * @returns {Position}
     */
    limit(range, velocity, outline) {
        return new LimitPosition(this, range, velocity, outline);
    }

    /**
     * @param {Object} obj
     * @param {string} propertyName
     * @returns {Position}
     */
    static create(obj, propertyName) {
        return new SimplePosition(obj, propertyName);
    }
}

export class SimplePosition extends Position {
    /**
     * @param {Object} obj
     * @param {string} propertyName
     */
    constructor(obj, propertyName) {
        super();

        this._obj = obj;
        this._propertyName = propertyName;
    }

    get value() {
        return this._obj[this._propertyName];
    }

    set value(value) {
        this._obj[this._propertyName] = value;
    }
}

export class LimitPosition extends Position {
    /**
     * @param {Position} parent
     * @param {{low: {number}, high: {number}}} range
     * @param {Function} outline
     * @param {Velocity} velocity
     */
    constructor(parent, range, velocity, outline) {
        super();

        this._parent = parent;
        this._range = range;
        this._velocity = velocity;
        this._outline = outline;
    }

    get value() {
        return this._parent.value;
    }

    set value(value) {
        this._parent.value = this.fit(value);
    }

    fit(value) {
        if (value < this._range.low) {
            this._velocity.reset();
            this._outline({low: true, high: false});
            return this._range.low;
        }
        if (value > this._range.high) {
            this._velocity.reset();
            this._outline({low: false, high: true});
            return this._range.high;
        }
        this._outline({low: false, high: false});
        return value;
    }
}

export class Position2D {
    /**
     * @returns {Position}
     */
    get x() {
        throw new Error('Require implement.');
    }

    /**
     * @returns {Position}
     */
    get y() {
        throw new Error('Require implement.');
    }

    /**
     * @param {Rectangle} range
     * @param {Velocity2D} velocity
     * @param {Function} outline
     * @returns {Position2D}
     */
    limit(range, velocity, outline) {
        return new LimitPosition2D(this, range, velocity, outline);
    }

    /**
     * @param {Object} obj
     * @returns {Position2D}
     */
    static create(obj) {
        return new SimplePosition2D(obj);
    }
}

export class SimplePosition2D extends Position2D {
    /**
     * @param {Object} obj
     */
    constructor(obj) {
        super();

        this._x = Position.create(obj, 'x');
        this._y = Position.create(obj, 'y');
    }

    /**
     * @returns {Position}
     */
    get x() {
        return this._x;
    }

    /**
     * @returns {Position}
     */
    get y() {
        return this._y;
    }
}

export class LimitPosition2D extends Position2D {
    /**
     * @param {Position2D} parent
     * @param {Rectangle} movable
     * @param {Velocity2D} velocity
     * @param {Function} outline
     */
    constructor(parent, movable, velocity, outline) {
        super();

        this._status = {
            left: false,
            top: false,
            right: false,
            bottom: false
        };
        this._outline = outline;

        this._x = parent.x.limit(movable.horizontal, velocity.x, ol => this.callbackX(ol));
        this._y = parent.y.limit(movable.vertical, velocity.y, ol => this.callbackY(ol));
    }

    get x() {
        return this._x;
    }

    get y() {
        return this._y;
    }

    callbackX(ol) {
        Object.assign(this._status, {left: ol.low, right: ol.high});
        this._outline(this._status);
    }

    callbackY(ol) {
        Object.assign(this._status, {bottom: ol.low, top: ol.high});
        this._outline(this._status);
    }
}