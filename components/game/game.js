import Size from "./size.js";
import GameRenderer from "./game-renderer.js";
import GameCamera from "./game-camera.js";
import GameScene from "./game-scene.js";
import GamePlayer from "./game-player.js";
import Rectangle from "./rectangle.js";
import {MultiInput} from "./input.js";

export default class Game {
    constructor(document, $canvas) {
        const size = Size.from($canvas);

        this._input = new MultiInput(document);
        this._renderer = new GameRenderer($canvas, size);
        this._camera = new GameCamera(size);
        this._scene = new GameScene();
        this._player = new GamePlayer(Rectangle.from(size));

        this._scene.add(this._player);
    }

    async initialize() {
        await this._player.initialize();
    }

    render() {
        this._player.update(this._input);
        this._renderer.render(this._scene, this._camera);
    }
}