import {Scene} from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/92/three.module.js';

export default class GameScene {
    constructor() {
        this._scene = new Scene();
    }

    /**
     * @returns {GameScene}
     */
    get scene() {
        return this._scene;
    }

    /**
     * @param {GameObject} obj
     */
    add(obj) {
        this._scene.add(obj.mesh);
    }
}