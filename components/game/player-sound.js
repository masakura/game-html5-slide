import SoundEffect from "./sound/sound-effect.js";

export default class PlayerSound {
    /**
     * @param {GamePlayer} player
     */
    constructor(player) {
        this._player = player;

        const context = new AudioContext();
        this._se = new SoundEffect(context);
    }

    async initialize() {
        await this._se.initialize();
    }

    /**
     * @param {Input} input
     */
    update(input) {
        if (input.horizontal && this._player.bottom) this._se.walking();
        if (input.jump) this._se.swimming();
    }
}