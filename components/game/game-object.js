export default class GameObject {
    /**
     * @returns {Mesh}
     */
    get mesh() {
        throw new Error('Require implement');
    }

    /**
     * @param {Input} input
     */
    update(input) {
        throw new Error('Require implement');
    }

    /**
     * @param {Offset} offset
     */
    move(offset) {
        throw new Error('Require implement');
    }
}