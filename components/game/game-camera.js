import {OrthographicCamera, PerspectiveCamera} from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/92/three.module.js';

export default class GameCamera {
    /**
     * @param size {Size}
     */
    constructor(size) {
        this._camera = GameCamera.orthographic(size);
    }

    /**
     * @param {Size} size
     */
    static perspective(size) {
        const camera = new PerspectiveCamera(45, size.aspect);
        camera.position.set(0, 0, 1000);
        return camera;
    }

    /**
     * @param {Size} size
     */
    static orthographic(size) {
        const half = size.half();
        const camera = new OrthographicCamera(-half.width, half.width,half.height, -half.height, 1, 1000);
        camera.position.set(0, 0, 1000);
        return camera;
    }

    /**
     * @returns {OrthographicCamera}
     */
    get camera() {
        return this._camera;
    }
}