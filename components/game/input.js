export class Input {
    /**
     * @returns {number}
     */
    get horizontal() {
        throw new Error('Require implement');
    }

    /**
     * @returns {boolean}
     */
    get jump() {
        throw new Error('Require implement');
    }
}

class InputBase extends Input {
    get horizontal() {
        if (this._left && this._right) return 0;
        if (this._left) return -1;
        if (this._right) return 1;
        return 0;
    }

    get jump() {
        return this._jump;
    }
}

export class GamePad extends InputBase {
    constructor(navigator) {
        super();

        this._navigator = navigator;
    }

    get horizontal() {
        const pad = this.getGamepad();
        if (!pad) {
            this._left = false;
            this._right = false;
        } else {
            this._left = pad.axes[0] < (this._center - 0.1);
            this._right = pad.axes[0] > (this._center + 0.1);
        }

        return super.horizontal;
    }

    get jump() {
        const pad = this.getGamepad();
        if (!pad) {
            this._jump = false;
        } else {
            this._jump = pad.buttons[0].pressed;
        }

        return super.jump;
    }

    getGamepad() {
        const pads = this._navigator.getGamepads();
        const actives = Array.from(pads)
            .filter(pad => pad)
            .filter(pad => pad.buttons.length > 2);

        const active = actives.length > 0 ? actives[0] : false;
        if (active) {
            if (!this._center) this._center = active.axes[0];
        } else {
            this._center = false;
        }
        return active;
    }
}

export class Keyboard extends InputBase {
    constructor(document) {
        super();

        document.addEventListener('keydown', e => this.keydown(e));
        document.addEventListener('keyup', e => this.keyup(e));

        this._right = false;
        this._left = false;
        this._jump = false;
    }

    keydown(e) {
        switch (e.key) {
            case 'ArrowLeft': this._left = true; break;
            case 'ArrowRight': this._right = true; break;
            case ' ': this._jump = true; break;
            default: return;
        }
        e.preventDefault();
        return false;
    }

    keyup(e) {
        switch (e.key) {
            case 'ArrowLeft': this._left = false; break;
            case 'ArrowRight': this._right = false; break;
            case ' ': this._jump = false; break;
            default: return;
        }
        e.preventDefault();
        return false;
    }

    get horizontal() {
        if (this._left && this._right) return 0;
        if (this._left) return -1;
        if (this._right) return 1;
        return 0;
    }

    get jump() {
        return this._jump;
    }
}

export class MultiInput extends Input{
    constructor(document) {
        super();

        this._keyboard = new Keyboard(document);
        this._gamepad = new GamePad(document.defaultView.navigator);
    }

    get horizontal() {
        const keyboard = this._keyboard.horizontal;
        const gamepad = this._gamepad.horizontal;

        if (!gamepad) return keyboard;
        if (!keyboard) return gamepad;
        if (gamepad === keyboard) return gamepad;
        return 0;
    }

    get jump() {
        const keyboard = this._keyboard.jump;
        const gamepad = this._gamepad.jump;

        return keyboard || gamepad;
    }
}