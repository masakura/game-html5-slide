export default class AnimationManager {
    constructor(texture) {
        this._texture = texture;
    }

    /**
     * @param {Ing} animation
     * @param {Direction} direction
     * @returns {boolean}
     */
    generate(animation, direction) {
        if (direction === this._direction && this._animation === animation) return false;
        if (direction) this._direction = direction;

        this._animation = animation;
        return true;
    }

    update() {
        this._direction.setRepeat(this._texture);
        this._direction.update(this._texture, this._animation.next());
    }
}