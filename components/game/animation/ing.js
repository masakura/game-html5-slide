export class Ing {
    constructor(start, end) {
        this._start = start;
        this._end = end;
        this._step = this._start;
    }

    next() {
        const current = this._step;

        this._step++
        if (this._step > this._end) this._step = this._start;

        return current;
    }
}

export class Full extends Ing {
    constructor() {
        super(0, 5);
    }
}

export class Resting extends Ing {
    next() {
        return 0;
    }
}

export class Walking extends Ing {
    constructor() {
        super(3, 5);
    }
}

export class Swiming extends Ing {
    constructor() {
        super(1, 2);
    }

    next() {
        return super.next();
    }
}