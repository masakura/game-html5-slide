import {Negative, Positive} from "./direction.js";
import {Resting, Swiming, Walking} from "./ing.js";
import AnimationManager from "./animation-manager.js";

export default class TextureAnimation {
    constructor(texture) {
        this._interval = 10;
        this._tick = 0;
        this._step = 0;

        this.negative = new Negative(6);
        this.positive = new Positive(6);

        this._walking = new Walking();
        this._swiming = new Swiming();
        this._resting = new Resting();

        this._manager = new AnimationManager(texture);

        this._direction = this.positive;
        this.rest();
    }

    update() {
        if (this._tick === 0) this.tick();

        this._tick++;
        if (this._tick >= this._interval)this._tick = 0;
    }

    tick() {
        this._manager.update();
        this._step = (this._step + 1) % 3;
    }

    rest() {
        this.animate(this._resting);
    }

    /**
     * @param {boolean} negative
     */
    walk(negative) {
        this.animate(this._walking, this.direction(negative));
    }

    /**
     * @param {boolean} negative
     */
    swim(negative) {
        this.animate(this._swiming, this.direction(negative));
    }

    animate(type, direction) {
        if (direction) this._direction = direction;
        if (this._manager.generate(type, this._direction)) this._tick = 0;
    }

    direction(negative) {
        return negative ? this.negative : this.positive;
    }
}