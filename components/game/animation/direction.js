export class Direction {
    constructor(count) {
        this._count = count;
    }

    setRepeat(texture) {
        texture.repeat.set(this.direction / this._count, 1);
    }

    update(texture, position) {
        texture.offset.x = this.index(this._count, position) / this._count;
    }

    get direction() {
        throw new Error('Require implement');
    }

    index(count, position) {
        throw new Error('Require implement');
    }
}

export class Negative extends Direction {
    constructor(count) {
        super(count);
    }

    get direction() {
        return -1;
    }

    index(count, position) {
        return -count + position + 1;
    }
}

export class Positive extends Direction {
    constructor(count) {
        super(count);
    }

    get direction() {
        return 1;
    }

    index(count, position) {
        return position;
    }
}