import GameObject from "./game-object.js";
import PlayerAnimation from "./player-animation.js";
import PlayerSprite from "./player-sprite.js";
import PlayerSound from "./player-sound.js";
import {Position2D} from "./position/position.js";
import {Velocity2D} from "./position/velocity.js";

class PlayerController {
    /**
     * @param {Velocity2D} velocity
     */
    constructor(velocity) {
        this._velocity = velocity;
    }

    update(input) {
        this._velocity.x.add(input.horizontal * 0.2);
        if (input.jump) this._velocity.y.value = 5;
    }
}

export default class GamePlayer extends GameObject {
    /**
     * @param {Rectangle} movable
     */
    constructor(movable) {
        super();

        this._sprite = new PlayerSprite();

        this._animation = new PlayerAnimation(this);
        this._sound = new PlayerSound(this);

        this.bottom = false;

        this._velocity = Velocity2D.create()
            .limit({x: 5, y: 0})
            .resistor({x: 0.1, y: 0})
            .gravity(0.3)
            .bindPosition(() => this._position);
        this._position = Position2D.create(this._sprite.mesh.position)
            .limit(movable.shrink(this._sprite.size), this._velocity, outline => this.bottom = outline.bottom);

        this._controller = new PlayerController(this._velocity);
    }

    async initialize() {
        await this._sound.initialize();
    }

    /**
     * @returns {Mesh}
     */
    get mesh() {
        return this._sprite.mesh;
    }

    /**
     * @param {Input} input
     */
    update(input) {
        this._controller.update(input);
        this._velocity.update();
        this._animation.update(input);
        this._sound.update(input);
        this._sprite.update();
    }

    /**
     * @param {boolean} negative
     */
    walk(negative) {
        this._sprite.walk(negative);
    }

    /**
     * @param {boolean} negative
     */
    swim(negative) {
        this._sprite.swim(negative);
    }

    rest() {
        this._sprite.rest();
    }

    rotate(horizontal) {
        this.mesh.rotation.y += horizontal * 0.1;
    }
}