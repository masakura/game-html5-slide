import {WebGLRenderer} from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/92/three.module.js';

export default class GameRenderer {
    /**
     * @param $canvas
     * @param {Size} size
     */
    constructor($canvas, size) {
        const renderer = new WebGLRenderer({canvas: $canvas});
        renderer.setPixelRatio(window.devicePixelRatio);
        // renderer.setSize(size.width, size.height);
        this._renderer = renderer;
    }

    /**
     * @param {GameScene} scene
     * @param {GameCamera} camera
     */
    render(scene, camera) {
        this._renderer.render(scene.scene, camera.camera);
    }
}