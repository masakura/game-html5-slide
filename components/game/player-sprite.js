import {
    TextureLoader,
    RepeatWrapping,
    SpriteMaterial,
    Sprite
} from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/92/three.module.js';
import TextureAnimation from "./animation/texture-animation.js";
import Size from "./size.js";

export default class PlayerSprite {
    constructor() {
        const texture = new TextureLoader().load('images/kappa.png');
        texture.wrapS = RepeatWrapping;
        texture.wrapT = RepeatWrapping;
        const material = new SpriteMaterial({
            map: texture
        });
        const sprite = new Sprite(material);
        this._mesh = sprite;

        texture.repeat.set(1 / 6, 1);
        sprite.scale.set(128, 128, 1);

        this._actions = new TextureAnimation(texture);
        this.size = new Size(128, 128);
    }

    get mesh() {
        return this._mesh;
    }

    /**
     * @param {boolean} negative
     */
    walk(negative) {
        this._actions.walk(negative);
    }

    /**
     * @param {boolean} negative
     */
    swim(negative) {
        this._actions.swim(negative);
    }

    rest() {
        this._actions.rest();
    }

    update() {
        this._actions.update();
    }
}