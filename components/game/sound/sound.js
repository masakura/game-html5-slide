export default class Sound {
    constructor(context, sound) {
        this._context = context;
        this._sound = sound;
    }

    play() {
        if (this._playing) return;

        const source = this._context.createBufferSource();
        source.buffer = this._sound;
        source.connect(this._context.destination);
        this._playing = true;
        source.onended = () => this._playing = false;
        source.start(0);
        this._source = source;
    }

    stop() {
        this._source && this._source.stop();
        this._source = false;
    }
}