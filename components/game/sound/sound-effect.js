import SoundResource from "./sound-resource.js";

export default class SoundEffect {
    constructor(context) {
        this._resource = new SoundResource(context);
    }

    async initialize() {
        await this._resource.initialize();
    }

    walking() {
        this._resource.swimming.stop();
        this._resource.walking.play();
    }

    swimming() {
        this._resource.walking.stop();
        this._resource.swimming.play();
    }
}