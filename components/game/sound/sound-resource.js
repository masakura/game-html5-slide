import Sound from "./sound.js";

export default class SoundResource {
    constructor(context) {
        this._context = context;
    }

    async initialize() {
        this.walking = await this.sound('audio/walking.mp3');
        this.swimming = await this.sound('audio/swimming.mp3');
    }

    async sound(file) {
        return new Sound(this._context, await this.fetch(file));
    }

    async fetch(file) {
        const response = await fetch(file);
        const buffer = await response.arrayBuffer();
        return await this._context.decodeAudioData(buffer);
    }
}