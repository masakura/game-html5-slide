import Game from "./game.js";

class Bootstrap {
    constructor(document, $canvas) {
        this._game = new Game(document, $canvas);
    }

    async initialize() {
        await this._game.initialize();
    }

    render() {
        this._game.render();
    }

    start() {
        const loop = () => {
            requestAnimationFrame(loop);
            this.render();
        };

        loop();
    }
}

(async () => {
    const app = {
        get isInitialized() {
            return !!this.bootstrap;
        },
        async initialize() {
            if (this.isInitialized) return;

            const bootstrap = new Bootstrap(document, document.querySelector('#canvas'));
            this.bootstrap = bootstrap;
            await bootstrap.initialize();
        },
        render() {
            if (this.isInitialized) this.bootstrap.render();
        }
    };

    const renderer = {
        initialize() {
        },
        render() {
            app.render();
        }
    };

    new Delay(renderer).bind(window);

    await audioConfirm({
        state: 'suspended',
        resume() {
            return Promise.resolve();
        },
    }, async () => app.initialize());
})();
