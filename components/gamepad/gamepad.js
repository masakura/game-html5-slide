class Button {
    constructor(index) {
        const button = document.createElement('button');
        button.classList.add('btn');
        button.classList.add('btn-primary');
        button.textContent = index + 1;
        this.element = button;
        this._index = index;
    }

    notify(pad) {
        if (this.pressed(pad)) {
            this.active();
        } else {
            this.deactive();
        }
    }

    pressed(pad) {
        return pad.buttons[this._index].pressed;
    }

    active() {
        this.element.classList.add('active')
    }

    deactive() {
        this.element.classList.remove('active')
    }

    static count(pad) {
        return pad.buttons.length;
    }

    static className() {
        return 'buttons';
    }
}

class AxeSlider {
    constructor() {
        const slider = document.createElement('input');
        slider.type = 'range';
        slider.min = -1000;
        slider.max = 1000;

        this.element = slider;
    }

    notifyValue(value) {
        this.element.value = value * 1000;
    }
}

class AxeValue {
    constructor() {
        this.element = document.createElement('span');
    }

    notifyValue(value){
        this.element.textContent = value;
    }
}

class Axe {
    constructor(index) {
        const element = document.createElement('div');
        element.classList.add('axe');

        const nodes = [new AxeSlider(), new AxeValue()];
        nodes.forEach(node => element.appendChild(node.element));

        this.element = element;
        this._nodes = nodes;
        this._index = index;
    }

    notify(pad) {
        const value = this.value(pad);
        this._nodes.forEach(node => node.notifyValue(value));
    }

    value(pad) {
        return pad.axes[this._index];
    }

    static count(pad) {
        return pad.axes.length;
    }

    static className() {
        return 'axes';
    }
}

class NodesPanel {
    constructor(pad, node) {
        const element = document.createElement('div');
        element.classList.add(node.className());

        const nodes = [];
        const count = node.count(pad);
        for (let index = 0; index < count; index++) {
            nodes.push(new node(index));
        }

        nodes.forEach(node => element.appendChild(node.element));

        this.element = element;
        this.nodes = nodes;
    }

    notify(pad) {
        this.nodes.forEach(node => node.notify(pad));
    }
}

class Panel {
    constructor(element, pad) {
        element.textContent = null;

        const nodes = [new NodesPanel(pad, Button), new NodesPanel(pad, Axe)];

        nodes.forEach(node => element.appendChild(node.element));

        this._nodes = nodes;
        this.element = element;
    }

    notify(pad) {
        this._nodes.forEach(node => node.notify(pad));
    }

    clear() {
        this.element.textContent = null;
        this.element = null;
        this._nodes = null;
    }
}

class PanelFactory {
    constructor(element) {
        this._cache = {};
        this.element = element;

        this.showRequireConnect();
    }

    initialize() {
    }

    render() {
        this.bind(navigator.getGamepads())
    }

    create(pad) {
        return this._cache[pad.id] = (this._cache[pad.id] || new Panel(this.element, pad));
    }

    clear() {
        const values = Object.values(this._cache);
        if (values.length <= 0) return false;

        Object.values(this._cache).forEach(panel => panel.clear());
        this._cache = {};
        this.showRequireConnect();
        return true;
    }

    showRequireConnect() {
        const element = document.createElement('p');
        element.textContent = 'パッドをつないでボタンを押してください';

        const children = Array.from(this.element.childNodes);
        children.forEach(child => this.element.removeChild(child));
        this.element.appendChild(element);
    }

    bind(pads) {
        const pad = this.getActive(pads);

        if (pad) {
            this.create(pad).notify(pad);
        } else {
            this.clear();
        }
    }

    getActive(pads) {
        const actives = Array.from(pads)
            .filter(pad => pad)
            .filter(pad => pad.buttons.length > 2);

        return actives.length > 0 ? actives[0] : null;
    }

    start() {
        const that = this;

        const loop = () => {
            that.bind(navigator.getGamepads())

            requestAnimationFrame(loop);
        };

        loop();
    }
}