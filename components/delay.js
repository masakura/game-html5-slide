class Delay {
    constructor(renderer) {
        this._renderer = renderer;
    }

    start() {
        if (!this._initialized) {
            this._renderer.initialize();
            this._initialized = false;
        }

        this._renderer.start && this._renderer.start();

        const that = this;
        that._running = true;

        const loop = () => {
            if (!that._running) return;

            that._renderer.render();

            requestAnimationFrame(loop);
        };

        loop();
    }

    stop() {
        this._running = false;
        this._renderer.stop && this._renderer.stop();
    }

    bind(context) {
        if (context === parent) this.start();

        context.addEventListener('message', e => {
            switch (e.data) {
                case 'slide:start': this.start(); break;
                case 'slide:stop': this.stop(); break;
            }
        });
    }
}