(async () => {
    class Graph {
        constructor(element) {
            this._context = element.getContext('2d');
        }

        draw(data) {
            const context = this._context;

            context.fillStyle = 'rgb(0, 0, 0)';
            context.fillRect(0, 0, 256, 256);

            context.fillStyle = 'rgb(255, 255, 0)';
            for (let i = 0; i < data.length; i++) {
                context.fillRect(i, 256 - data[i], 1, data[i]);
            }
        }
    }

    class Analyser {
        constructor(context) {
            this._analyser = context.createAnalyser();
            this._analyser.fftSize = 1024;
        }

        getData() {
            const data = new Uint8Array(256);
            this._analyser.getByteFrequencyData(data);
            return data;
        }

        get destination() {
            return this._analyser;
        }
    }

    class Recorder {
        constructor(context, input) {
            const source = context.createMediaStreamSource(input);
            const analyser = new Analyser(context);
            const delay = context.createDelay();
            delay.delayTime.value = 0.2;
            const gain = context.createGain();
            gain.gain.value = 0;

            source.connect(analyser.destination);
            source.connect(delay);
            delay.connect(gain);
            gain.connect(context.destination);

            this.analyser = analyser;
            this._gain = gain;
        }

        volume(value) {
            this._gain.gain.value = value;
        }
    }

    const context = new AudioContext();

    const app = {
        get isInitialized() {
            return !!this.graph;
        },
        async initialize() {
            if (this.isInitialized) return;

            const graph = new Graph(document.querySelector('#canvas'));

            const input = await navigator.mediaDevices.getUserMedia({audio: true});
            const recorder = new Recorder(context, input);

            const $volume = document.querySelector('#volume');
            $volume.value = 0;
            $volume.addEventListener('input', () => recorder.volume($volume.value));

            this.graph = graph;
            this.recorder = recorder;
        },
        render() {
            if (this.isInitialized) this.graph.draw(this.recorder.analyser.getData());
        }
    };

    const delay = new Delay({
        initialize() {
        },

        render() {
            app.render();
        }
    });
    delay.bind(window);

    await audioConfirm(context, async () => app.initialize());
})();
