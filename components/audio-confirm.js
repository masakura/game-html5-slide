const audioConfirm = async (context, action) => {
    action = action || (() => Promise.resolve());

    if (context.state !== 'suspended') {
        await action();
        return;
    }

    const confirm = document.querySelector('#confirm');
    confirm.style.display = 'flex';

    confirm.addEventListener('click', async () => {
        confirm.style.display = 'none';
        await context.resume();
        await action();
    });
};
