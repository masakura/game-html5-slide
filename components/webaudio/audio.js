class Destination {
    constructor(context) {
        const compressor = context.createDynamicsCompressor();
        compressor.connect(context.destination);
        this.destination = compressor;
    }
}

class Channel {
    constructor(context, freq) {
        const oscillator = context.createOscillator();
        oscillator.type = 'sine';
        oscillator.frequency.value = freq;

        const gain = context.createGain();
        gain.gain.value = 0;

        oscillator.connect(gain);

        oscillator.start(0);

        this._gain = gain;
    }

    up() {
        this._gain.gain.value = 0;
    }

    down() {
        this._gain.gain.value = 0.8;
    }

    connect(destination) {
        this._gain.connect(destination);
    }
}

class ChannelButton {
    constructor(context, element) {
        this._freq = parseFloat(element.dataset.freq);
        this._context = context;
        this._element = element;
    }

    get channel() {
        return this._channel = (this._channel || new Channel(this._context, this._freq));
    }

    is(element) {
        return this._element === element;
    }

    up() {
        this.channel.up();
        this._element.classList.remove('pressed');
    }

    down() {
        this._element.classList.add('pressed');
        this.channel.down();
    }

    connect(destination) {
        this.channel.connect(destination);
    }
}

class OverrappedButton {
    constructor(parent) {
        this._parent = parent;
        this._level = 0;
    }

    is(element) {
        return this._parent.is(element);
    }

    up() {
        if (this._level > 0) {
            this._level--;
        }

        if (this._level > 0) return;

        this._parent.up();
    }

    down() {
        this._level++;

        if (this._level > 1) return;

        this._parent.down();
    }

    connect(destination) {
        this._parent.connect(destination);
    }
}

class NullButton {
    up() {
    }

    down() {
    }
}
NullButton.instance = new NullButton();

class ButtonCollection {
    constructor(context, element) {

        this._buttons = Array.from(element.querySelectorAll('.key > *'))
            .map(key => ButtonCollection.create(context, key));
    }

    static create(context, element) {
        return new OverrappedButton(new ChannelButton(context, element));
    }

    connect(destination) {
        this._buttons.forEach(button => button.connect(destination));
    }

    findByPosition(position) {
        const element = document.elementFromPoint(position.clientX, position.clientY);
        return this.findByElement(element);
    }

    findByElement(element) {
        const filtered = this._buttons.filter(button => button.is(element));

        return filtered.length > 0 ? filtered[0] : NullButton.instance;
    }
}

class Touch {
    constructor() {
        this._button = NullButton.instance;
    }

    up() {
        this._button.up();
        this._button = NullButton.instance;
    }

    down(button) {
        button.down();
        this._button = button;
    }

    move(button) {
        if (this._button === button) return;
        this.up();
        this.down(button);
    }
}

class TouchProvider {
    constructor() {
        this._touches = {};
    }

    find(identifier) {
        return this._touches[identifier] = (this._touches[identifier] || new Touch());
    }

    up(identifier, button) {
        const touch = this.find(identifier);
        touch.up(button);
    }

    down(identifier, button) {
        const touch = this.find(identifier);
        touch.down(button);
    }

    move(identifier, button) {
        const touch = this.find(identifier);
        touch.move(button);
    }
}

class TouchNavigator {
    constructor(buttons) {
        this._buttons = buttons;
        this._provider = new TouchProvider();

        document.addEventListener('touchstart', e => this.touchstart(e), false);
        document.addEventListener('touchend', e => this.touchend(e), false);
        document.addEventListener('touchmove', e => this.touchmove(e), false);
    }

    touchstart(e) {
        for (let touch of e.touches) {
            this.move(touch);
        }
    }

    touchend(e) {
        e.stopPropagation();

        for (let touch of e.changedTouches) {
            this.up(touch);
        }
    }

    touchmove(e) {
        e.stopPropagation();

        for (let touch of e.touches) {
            this.move(touch);
        }
    }

    down(touch) {
        const button = this._buttons.findByPosition(touch);
        this._provider.down(touch.identifier, button);
    }

    up(touch) {
        const button = this._buttons.findByPosition(touch);
        this._provider.up(touch.identifier, button);
    }

    move(touch) {
        const button = this._buttons.findByPosition(touch);
        this._provider.move(touch.identifier, button);
    }
}

class MouseNavigator {
    constructor(buttons) {
        this._buttons = buttons;
        this._touch = new Touch();

        document.addEventListener('mousedown', e => this.mousedown(e), false);
        document.addEventListener('mouseup', e => this.mouseup(e), false);
        document.addEventListener('mouseover', e => this.mouseover(e), false);
        document.addEventListener('mouseout', e => this.mouseout(e), false)
    }

    mousedown(e) {
        const button = this._buttons.findByPosition(e);
        this._touch.down(button);
    }

    mouseup(e) {
        const button = this._buttons.findByPosition(e);
        this._touch.up(button);

        return false;
    }

    mouseover(e) {
        if (!e.buttons) return;

        const button = this._buttons.findByPosition(e);
        this._touch.down(button);
    }

    mouseout(e) {
        if (!e.buttons) return;

        const button = this._buttons.findByPosition(e);
        this._touch.up(button);
    }
}