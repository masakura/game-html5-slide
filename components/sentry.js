(() => {
    const sentryDsn = '';
    if (sentryDsn) {
        Raven.config(sentryDsn, {
            environment: '',
            tags: {
                git_commit: '',
                gitlab_environment: '',
            }
        }).install()
    }
})();
